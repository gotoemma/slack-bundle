<?php

namespace Gotoemma\SlackBundle\DependencyInjection;

use Gotoemma\SlackBundle\Action\WebhookAction;
use Gotoemma\SlackBundle\EventListener\OpenDialogEventListener;
use Gotoemma\SlackBundle\Service\CommandResolver;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;

class SlackExtension extends Extension
{
	/**
	 * {@inheritDoc}
	 */
	public function load(array $configs, ContainerBuilder $container)
	{
		$configuration = new Configuration();
		$processedConfiguration = $this->processConfiguration($configuration, $configs);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        $container
            ->register(WebhookAction::class, WebhookAction::class)
            ->setAutowired(true);

		$serviceDefinition = $container->getDefinition(CommandResolver::class);
		$serviceDefinition->addMethodCall('setConfiguration', array($processedConfiguration));

        $serviceDefinition = $container->getDefinition(OpenDialogEventListener::class);
        $serviceDefinition->addMethodCall('setConfiguration', array($processedConfiguration));

        $container->setParameter('slack.client_id',$processedConfiguration['client_id']);
        $container->setParameter('slack.client_secret',$processedConfiguration['client_secret']);
        $container->setParameter('slack.client_token',$processedConfiguration['client_token']);
        $container->setParameter('slack.token',$processedConfiguration['token']);
	}
}
