<?php

namespace Gotoemma\SlackBundle\DependencyInjection\Compiler;

use Gotoemma\SlackBundle\Provider\CommandProviderInterface;
use Gotoemma\SlackBundle\Service\CommandResolver;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class CommandProviderPass implements CompilerPassInterface
{

    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition(CommandResolver::class)) {
            return;
        }

        $definition = $container->findDefinition(CommandResolver::class);

        $taggedServices = $container->findTaggedServiceIds(CommandProviderInterface::SERVICE_TAG);

        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('addCommand', array(new Reference($id)));
        }
    }
}