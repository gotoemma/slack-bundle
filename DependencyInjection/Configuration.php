<?php
namespace Gotoemma\SlackBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
	public function getConfigTreeBuilder()
	{
		$treeBuilder = new TreeBuilder();
		$treeBuilder
			->root('slack')
			->children()
				->scalarNode('client_id')->end()
                ->scalarNode('client_secret')->end()
                ->scalarNode('client_token')->end()
                ->scalarNode('token')->end()
			->end();

		return $treeBuilder;
	}
}
