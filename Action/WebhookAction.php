<?php

namespace Gotoemma\SlackBundle\Action;

use Gotoemma\SlackBundle\Dto\Message;
use Gotoemma\SlackBundle\Service\CommandResolver;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class WebhookAction
{
    const PAYLOAD_TYPE_DIALOG_SUBMISSION = "dialog_submission";

    /**
     * @var CommandResolver
     */
	private $commandResolver;

	public function __construct(CommandResolver $commandResolver) {
		$this->commandResolver = $commandResolver;
	}

	/**
	 * @param Request $request
	 * @return Response
	 *
	 * @Route(
	 *     name="slack_webhook",
	 *     path="/webhook/slack",
	 * )
	 * @Method("POST")
	 */
	public function __invoke(Request $request)
	{
	    try {
	        // Transform dialog payload to parameters as received for slash commands
            if ($request->request->has('payload')) {
                $payload = json_decode($request->request->get('payload'), true);
                $request->request->add($payload);

                if ($payload['type'] === self::PAYLOAD_TYPE_DIALOG_SUBMISSION) {
                    $callback = json_decode($payload['callback_id'], true);
                    $request->request->add($callback);
                }
            }
	        $response = $this->commandResolver->handleRequest($request);
        } catch (\Exception $exception) {
	        $response = new Message($exception->getMessage());
        }

        // Return response with empty body if commandResolver returned true just to indicate it ran successfully
		return $response === true ? new Response() : new JsonResponse($response);
	}
}
