<?php

namespace Gotoemma\SlackBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Gotoemma\SlackBundle\Dto\Attachment;
use Gotoemma\SlackBundle\EventListener\DelayedResponseEventListener;
use Gotoemma\SlackBundle\Dto\Message;
use Gotoemma\SlackBundle\EventListener\OpenDialogEventListener;
use Gotoemma\SlackBundle\Provider\CommandProviderInterface;
use Gotoemma\SlackBundle\Provider\DialogProviderInterface;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class CommandResolver
{
    /**
     * @var array
     */
    private $configuration;

    /**
     * @var ArrayCollection
     */
    private $commands;

    /**
     * @var DelayedResponseEventListener
     */
    private $delayedResponseEventListener;

    /**
     * @var OpenDialogEventListener
     */
    private $openDialogEventListener;

    public function __construct(
        DelayedResponseEventListener $delayedResponseEventListener,
        OpenDialogEventListener $openDialogEventListener
    ) {
        $this->commands = new ArrayCollection();
        $this->delayedResponseEventListener = $delayedResponseEventListener;
        $this->openDialogEventListener = $openDialogEventListener;
    }

    /**
     * @param array $configuration
     */
    public function setConfiguration($configuration)
    {
        if (!isset($configuration['token'])) {
            throw new InvalidConfigurationException("slack.token is required configuration value.");
        }

        $this->configuration = $configuration;
    }

    /**
     * @param CommandProviderInterface $command
     */
    public function addCommand(CommandProviderInterface $command)
    {
        $this->commands->add($command);
    }

    /**
     * @param Request $request
     * @return Message
     */
    public function handleRequest(Request $request)
    {
        $this->validateRequest($request);
        return $this->handleCommand($this->getCommandFromRequest($request), $request->request->all());
    }

    /**
     * @param string $command
     * @param array $parameters
     * @return Message|bool
     *
     * @throws \Exception
     */
    private function handleCommand($command, array $parameters)
    {
        $attachments = new ArrayCollection();

        /** @var CommandProviderInterface $service */
        foreach ($this->getServicesForCommand($command, $parameters) AS $service) {
            if ($attachment = $service->performsDelayedResponse($command, $parameters)) {
                $this->delayedResponseEventListener->addToQueue($service, $command, $parameters, $service->injectRegisteredCommands() ? $this->commands : null);

                if ($attachment instanceof Attachment) {
                    $attachments->add($attachment);
                }
            } elseif ($service instanceof DialogProviderInterface && $service->opensDialog($command, $parameters)) {
                $this->openDialogEventListener->addToQueue($service, $command, $parameters, $service->injectRegisteredCommands() ? $this->commands : null);
            } elseif ($attachment = $service->handleCommand($command, $parameters, $service->injectRegisteredCommands() ? $this->commands : null)) {
                $attachments->add($attachment);
            }
        }

        if ($attachments->isEmpty() && empty($this->openDialogEventListener->getQueue()) && empty($this->delayedResponseEventListener->getQueue())) {
            throw new BadRequestHttpException(
                'Something went wrong while processing your request.'
            );
        } elseif ($attachments->isEmpty() && !empty($this->delayedResponseEventListener->getQueue())) {
            return true;
        } elseif ($attachments->isEmpty() && !empty($this->openDialogEventListener->getQueue())) {
            return true;
        }

        return new Message(
            null,
            $attachments->toArray(),
            Message::RESPONSE_TYPE_PUBLIC
        );
    }

    /**
     * @param string $command
     * @param array $parameters
     * @return bool
     */
    private function isSupportedCommand($command, array $parameters)
    {
        return !$this->getServicesForCommand($command, $parameters)->isEmpty();
    }

    /**
     * @param string $command
     * @param array $parameters
     * @return ArrayCollection
     */
    private function getServicesForCommand($command, array $parameters)
    {
        return $this->commands->filter(function (CommandProviderInterface $commandService) use ($command, $parameters) {
            return $commandService->supportsCommand($command, $parameters);
        });
    }

    /**
     * @param Request $request
     * @return mixed
     */
    private function getCommandFromRequest(Request $request)
    {
        return basename($this->getParameterFromRequest('command', $request));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    private function getTriggerIdFromRequest(Request $request)
    {
        return basename($this->getParameterFromRequest('trigger_id', $request));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    private function getTokenFromRequest(Request $request)
    {
        return $this->getParameterFromRequest('token', $request);
    }

    /**
     * @param string $parameter
     * @param Request $request
     * @return mixed
     */
    private function getParameterFromRequest($parameter, Request $request)
    {
        return $request->request->get($parameter);
    }

    public function validateRequest(Request $request)
    {
        if (!$this->getTokenFromRequest($request) || $this->getTokenFromRequest($request) !== $this->configuration['token']) {
            throw new UnauthorizedHttpException('token', 'Unauthorized access.');
        }
        if (!$this->getCommandFromRequest($request)) {
            throw new BadRequestHttpException('No command given.');
        }
        if (!$this->isSupportedCommand($this->getCommandFromRequest($request), $request->request->all())) {
            throw new BadRequestHttpException('Command not supported.');
        }
    }
}