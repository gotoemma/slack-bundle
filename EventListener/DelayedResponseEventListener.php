<?php

namespace Gotoemma\SlackBundle\EventListener;

use Doctrine\Common\Collections\ArrayCollection;
use Gotoemma\SlackBundle\Dto\Message;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Gotoemma\SlackBundle\Provider\CommandProviderInterface;
use Symfony\Component\HttpKernel\Event\KernelEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class DelayedResponseEventListener implements EventSubscriberInterface {

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var array
     */
    private $queue = [];

    /**
     * DelayedResponseEventListener constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::TERMINATE => 'sendDelayedResponse'
        ];
    }

    /**
     * @param $command
     */
    public function addToQueue(CommandProviderInterface $commandProvider, string $command, array $parameters, $registeredCommands = null)
    {
        $this->queue[] = [
          'commandProvider' => $commandProvider,
          'command' => $command,
          'parameters' => $parameters,
          'registeredCommands' => $registeredCommands
        ];
    }

    /**
     * @return array
     */
    public function getQueue()
    {
        return $this->queue;
    }

    public function sendDelayedResponse(KernelEvent $event)
    {
        $attachments = new ArrayCollection();

        foreach ($this->queue AS $item) {
            /** @var CommandProviderInterface $commandProvider */
            $commandProvider = $item['commandProvider'];
            if ($attachment = $commandProvider->handleCommand($item['command'], $item['parameters'], $item['registeredCommands'])) {
                $attachments->add($attachment);
            }

            $message = new Message(
                null,
                $attachments->toArray(),
                Message::RESPONSE_TYPE_PUBLIC
            );

            $buzz = $this->container->get('buzz');
            $buzz->post(
                $item['parameters']['response_url'],
                ['Content-type' => 'application/json'],
                json_encode($message)
            );
        }
    }
}
