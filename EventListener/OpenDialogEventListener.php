<?php

namespace Gotoemma\SlackBundle\EventListener;

use Doctrine\Common\Collections\ArrayCollection;
use Gotoemma\SlackBundle\Dto\DialogResponse;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Gotoemma\SlackBundle\Provider\CommandProviderInterface;
use Symfony\Component\HttpKernel\Event\KernelEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class OpenDialogEventListener implements EventSubscriberInterface {

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var array
     */
    private $queue = [];

    /**
     * @var array
     */
    private $configuration;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::TERMINATE => 'openDialog'
        ];
    }

    /**
     * @param array $configuration
     */
    public function setConfiguration($configuration)
    {

        if (!isset($configuration['client_token'])) {
            throw new InvalidConfigurationException("slack.client_token is required configuration value.");
        }

        $this->configuration = $configuration;
    }

    /**
     * @param $command
     */
    public function addToQueue(CommandProviderInterface $commandProvider, string $command, array $parameters, $registeredCommands = null)
    {
        $this->queue[] = [
          'commandProvider' => $commandProvider,
          'command' => $command,
          'parameters' => $parameters,
          'registeredCommands' => $registeredCommands
        ];
    }

    /**
     * @return array
     */
    public function getQueue()
    {
        return $this->queue;
    }

    public function openDialog(KernelEvent $event)
    {
        $dialogs = new ArrayCollection();

        foreach ($this->queue AS $item) {
            /** @var CommandProviderInterface $commandProvider */
            $commandProvider = $item['commandProvider'];
            if ($dialog = $commandProvider->handleCommand($item['command'], $item['parameters'], $item['registeredCommands'])) {

                $dialog->callback_id = json_encode(array_merge([], $dialog->callback_id, [
                    "command" => $item['parameters']['command'],
                ]));

                $data = json_encode(
                    new DialogResponse(
                        $item['parameters']['trigger_id'],
                        $dialog
                    )
                );

                $buzz = $this->container->get('buzz');
                $message = $buzz->post(
                    "https://slack.com/api/dialog.open",
                    [
                        'Content-type' => 'application/json',
                        'Charset' => 'UTF-8',
                        'Authorization' => 'Bearer ' . $this->configuration['client_token'],
                    ],
                    $data
                );
            }
        }
    }
}
