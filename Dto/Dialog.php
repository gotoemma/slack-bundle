<?php

namespace Gotoemma\SlackBundle\Dto;

use Gotoemma\SlackBundle\Dto\Element\Element;

class Dialog
{

    public $callback_id = [];
    public $title;
    public $submit_label;
    public $notify_on_cancel = false;

    /**
     * @var Element[]
     */
    public $elements;

    public function __construct($callback_id = null, $title = null, $submit_label = null, $notify_on_cancel = false, $elements = null)
    {
        $this->callback_id = $callback_id;
        $this->title = $title;
        $this->submit_label = $submit_label;
        $this->notify_on_cancel = $notify_on_cancel;
        $this->elements = $elements;
    }
}