<?php

namespace Gotoemma\SlackBundle\Dto;

class Message
{
    const RESPONSE_TYPE_PRIVATE = "ephemeral";
    const RESPONSE_TYPE_PUBLIC = "in_channel";

    public $response_type;
    public $text;

    /**
     * @var Attachment[]
     */
    public $attachments;

    public function __construct($text = null, $attachments = null, $response_type = self::RESPONSE_TYPE_PRIVATE)
    {
        $this->response_type = $response_type;
        $this->text = $text;
        $this->attachments = $attachments;
    }
}