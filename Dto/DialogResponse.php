<?php

namespace Gotoemma\SlackBundle\Dto;

class DialogResponse
{

    public $trigger_id;

    /**
     * @var Dialog
     */
    public $dialog;

    public function __construct($trigger_id = null, Dialog $dialog = null)
    {
        $this->trigger_id = $trigger_id;
        $this->dialog = $dialog;
    }
}