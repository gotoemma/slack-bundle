<?php

namespace Gotoemma\SlackBundle\Dto;

class Attachment
{
    public $pretext;
    public $text;
    public $color;
    public $title;
    public $title_link;

    /**
     * @var Field[]
     */
    public $fields;

    public function __construct($text = null, $pretext = null, $color = null, $title = null, $title_link = null, $fields = null)
    {
        $this->text = $text;
        $this->pretext = $pretext;
        $this->color = $color;
        $this->title = $title;
        $this->title_link = $title_link;
        $this->fields = $fields;
    }
}