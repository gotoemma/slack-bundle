<?php

namespace Gotoemma\SlackBundle\Dto;

class Field
{
    public $title;
    public $value;
    public $short = false;

    public function __construct($title = null, $value = null, $short = false)
    {
        $this->title = $title;
        $this->value = $value;
        $this->short = $short;
    }
}