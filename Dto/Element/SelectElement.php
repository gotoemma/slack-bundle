<?php

namespace Gotoemma\SlackBundle\Dto\Element;

class SelectElement extends Element
{
    /**
     * @var SelectOption[]
     */
    public $options;

    public function __construct($label = null, $name = null, $value = null, $placeholder = null, $hint = null, $optional = false, $options = null)
    {
        parent::__construct("select", $label, $name, $value, $placeholder, $hint, $optional);

        $this->options =  $options;
    }
}