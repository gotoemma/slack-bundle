<?php

namespace Gotoemma\SlackBundle\Dto\Element;

class TextareaElement extends Element
{
    public function __construct($label = null, $name = null, $value = null, $placeholder = null, $hint = null, $optional = false, $subtype = null)
    {
        parent::__construct("textarea", $label, $name, $value, $placeholder, $hint, $optional);

        if ($subtype) {
            $this->subtype = $subtype;
        }
    }
}