<?php

namespace Gotoemma\SlackBundle\Dto\Element;

class TextElement extends Element
{
    public function __construct($label = null, $name = null, $value = null, $placeholder = null, $hint = null, $optional = false, $subtype = null)
    {
        parent::__construct("text", $label, $name, $value, $placeholder, $hint, $optional);

        if ($subtype) {
            $this->subtype = $subtype;
        }
    }
}