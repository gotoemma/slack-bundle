<?php

namespace Gotoemma\SlackBundle\Dto\Element;

class Element
{
    public $type;
    public $label;
    public $name;
    public $value;
    public $placeholder;
    public $hint;
    public $optional = false;

    public function __construct($type = null, $label = null, $name = null, $value = null, $placeholder = null, $hint = null, $optional = false)
    {
        $this->type = $type;
        $this->label = $label;
        $this->name = $name;
        $this->value = $value;
        $this->placeholder = $placeholder;
        $this->hint = $hint;
        $this->optional = $optional;
    }
}