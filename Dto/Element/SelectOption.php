<?php

namespace Gotoemma\SlackBundle\Dto\Element;

class SelectOption
{
    public $label;
    public $value;

    public function __construct($label = null, $value = null)
    {
        $this->label = $label;
        $this->value = $value;
    }
}