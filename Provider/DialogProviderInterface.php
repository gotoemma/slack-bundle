<?php

namespace Gotoemma\SlackBundle\Provider;

interface DialogProviderInterface
{
    /**
     * Returns true if the CommandProvider will initiate a dialog and CommandResolver should announce that
     *
     * @param string $command
     * @param array $parameters
     * @return boolean
     */
    public function opensDialog(string $command, array $parameters);
}
