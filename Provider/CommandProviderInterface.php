<?php

namespace Gotoemma\SlackBundle\Provider;

use Doctrine\Common\Collections\ArrayCollection;
use Gotoemma\SlackBundle\Dto\Attachment;
use Gotoemma\SlackBundle\Dto\Dialog;
use Gotoemma\SlackBundle\Dto\Field;
use Gotoemma\SlackBundle\Dto\Message;

interface CommandProviderInterface
{
    const SERVICE_TAG = 'slack.command';

    /**
     * Return true if this CommandProviders handleCommand() method should be called for the given arguments
     *
     * @param string $command
     * @param array $parameters
     * @return boolean
     */
    public function supportsCommand(string $command, array $parameters);

    /**
     * Returns true if the CommandProvider will perform a delayed response and CommandResolver should announce that
     * You can also return a Message object to announce the delayed final response
     *
     * @param string $command
     * @param array $parameters
     * @return boolean|Message
     */
    public function performsDelayedResponse(string $command, array $parameters);

    /**
     * Return true if the registeredCommands argument should be set on handleCommand() call
     *
     * @return boolean
     */
    public function injectRegisteredCommands();

    /**
     * @param string $command
     * @param array $parameters
     * @param ArrayCollection $registeredCommands ArrayCollection of all CommandProviders that registered for SlackBundle
     * @return Attachment|Dialog
     */
    public function handleCommand(string $command, array $parameters, $registeredCommands = null);

    /**
     * Return a Field object containing some user instructions how to use your command
     * which will be shown in slack if one uses the ListCommand
     *
     * @return Field
     */
    public function getDescription();
}
