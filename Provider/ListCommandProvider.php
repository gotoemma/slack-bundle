<?php

namespace Gotoemma\SlackBundle\Provider;

use Doctrine\Common\Collections\ArrayCollection;
use Gotoemma\SlackBundle\Dto\Attachment;
use Gotoemma\SlackBundle\Dto\Field;

class ListCommandProvider implements CommandProviderInterface
{
    private static $commandName = 'list';

    public function supportsCommand(string $command, array $parameters)
    {
        return $command === self::$commandName;
    }

    public function performsDelayedResponse(string $command, array $parameters)
    {
       return false;
    }

    public function injectRegisteredCommands()
    {
        return true;
    }

    public function getDescription()
    {
        return new Field(
            "List",
            "/list\nShow a list of all commands."
        );
    }

    /**
     * @param string $command
     * @param array $parameters
     * @param ArrayCollection $registeredCommands
     * @return Attachment
     */
    public function handleCommand(string $command, array $parameters, $registeredCommands = null)
    {
        return new Attachment(
            "Find a list of all registered commands below.",
            null,
            '#4f4f4rf',
            'Command List',
            null,
            $registeredCommands->map(function (CommandProviderInterface $command) {
                $description = $command->getDescription();
                $description->short = false;
                return $description;
            })->toArray()
        );
    }
}