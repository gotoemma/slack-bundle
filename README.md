# SlackBundle - Basic implementation for Slack Slash Commands
This bundle provides the basic functionality to quickly connect your Slack Bot with your symfony project via slash commands. 

**Simply create a class, implement the provided CommandInterface and tag it as "slack.command" and your slash command is ready to go**!

SlackBundle validates the token sent with the request to ensure this request comes authorized and from Slack.
Then it searches for an service tagged with slack.command which tells to supported a given request. After collecting data from all suppported services it will respond to slack.

![The default list command](https://i.imgur.com/8tUa2fD.png")

## Dependencies

- Symfony 3.1
- PHP 7.0

## Setup

- Create your own command class and implement Gotoemma\SlackBundle\Service\CommandInterface
- Tag your service with "slack.command"
- Go to https://api.slack.com/apps and create an app
- Add the clientId, clientSecret and token to your the config.yml
- Setup your first slash command and set https://your.url/webhook/slack as webhook url (use POST request)

### Your projects main composer.json

```
"repositories": [
        {
            "type": "git",
            "url": "https://bitbucket.org/gotoemma/slack-bundle.git"
        }
    ],
```

Then type **composer require gotoemma/slack-bundle**

### app/config/config.yml

```
slack:
  client_id:      "%slack_client_id%"
  client_secret:  "%slack_client_secret%"
  token:          "%slack_token%"

```

### app/config/routing.yml

Load the routing for upload action that is configured in UploadAction class as annotation on __invoke() method.

```
slack_action:
    resource: '@SlackBundle/Action/'
    type:     'annotation'
```

### Your first command provider

With SlackBundle ships one default command: /list
This command lists all registered commands in slack. The only thing you need to do is to add the slash command on api.slack.com/apps.

This command can be used as a basis for your first custom command also.

```php
<?php

namespace YourNamespace\YourBundle\Provider;

use Doctrine\Common\Collections\ArrayCollection;
use Gotoemma\SlackBundle\Dto\Attachment;
use Gotoemma\SlackBundle\Dto\Field;
use Gotoemma\SlackBundle\Provider\CommandProviderInterface;

class YourCommandProvider implements CommandProviderInterface
{

    /**
     * Return true if this CommandProviders handleCommand() method should be called for the given arguments
     *
     * @param string $command
     * @param array $parameters
     * @return boolean
     */
    public function supportsCommand(string $command, array $parameters);

    /**
     * Return true if the registeredCommands argument should be set on handleCommand() call
     *
     * @return boolean
     */
    public function injectRegisteredCommands();

    /**
     * @param string $command
     * @param array $parameters
     * @param ArrayCollection $registeredCommands ArrayCollection of all CommandProviders that registered for SlackBundle
     * @return Attachment
     */
    public function handleCommand(string $command, array $parameters, $registeredCommands = null);

    /**
     * Return a Field object containing some user instructions how to use your command
     * which will be shown in slack if one uses the ListCommand
     *
     * @return Field
     */
    public function getDescription();
}

```